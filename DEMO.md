
- montrer `.gitpod.yml`

### Redis

```bash
sudo docker-compose up
docker exec -it redis-server /bin/sh
redis-cli
ping
```

### MQTT

```bash
node listen.js
node publish.js
```
